/*
 * ============ Package Declaration ==============
 */
package com.snyamathi.sensorlogger;

/*
 * =============== Imports =======================
 */
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import android.util.SparseArray;
import android.widget.Toast;

/*
 * =============== Data Collection Background Service =======================
 * 		- for both location-based and inertial-based sensors
 */
public class DataCollectionService extends Service implements LocationListener,SensorEventListener {
	// ----- Debugging String Tag -----
	public final static String LOG_TAG = "DataCollectionService";

	// ----- Wakelock -----
	private WakeLock mWakeLock;
	
	// ----- Output file -----
	private File mExternalRoot;
	private File mOutputDir;
	private File mOutputFile;
	private static String sLogFile = "RideKeeperLog";
	private static FileOutputStream sOutputStream;
	FileWriter fw;
	BufferedWriter bw;

	// ----- Record location and network information -----
	public static WifiInfo lastWifiInfo;
	public static GsmCellLocation lastCellInfo;
	public static Location lastLocation;

	// ----- Inertial Sensor Sampling Rate -----
	private static final int SENSOR_DELAY = SensorManager.SENSOR_DELAY_GAME;
	
	// ----- Inertial Sensor Types -----
	private SparseArray<Sensor> mSensors;
	private static final int[] SENSOR_TYPES = new int[] {
		Sensor.TYPE_ACCELEROMETER,
		// Sensor.TYPE_GAME_ROTATION_VECTOR,		
		Sensor.TYPE_GRAVITY,
		Sensor.TYPE_GYROSCOPE,
		Sensor.TYPE_LINEAR_ACCELERATION,
		Sensor.TYPE_MAGNETIC_FIELD,
		// Sensor.TYPE_ROTATION_VECTOR,
	};
	
	// ----- Temporary Sensor Storage -----
	private float[] tmp_orientationRotation = new float[9];
	private float[] tmp_orientationInclination = new float[9];
	private float[] tmp_gravity = new float[3];
	private float[] tmp_geomag = new float[3];
	private float[] tmp_eulerAngles = new float[3];
	
	// ----- Sensor Managers -----
	private SensorManager mSensorManager;
	private LocationManager mLocationManager;
	
	// ----- Binder Object -----
	private final IBinder mBinder = new LocalBinder();

	// ----- Binding Function -----
	public class LocalBinder extends Binder {
		DataCollectionService getService() {
			return DataCollectionService.this;
		}
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		Log.i(LOG_TAG, "in onBind of DataCollectionService");
		return mBinder;
	}
	
	// ----- Initialization of Service -----
	public void init() {
		// request appropriate services for sensor and location managers
		mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		
		// populate the correct sensors (if available) in an array
		mSensors = new SparseArray<Sensor>();
		for (int sensorType : SENSOR_TYPES) {
			Sensor sensor = mSensorManager.getDefaultSensor(sensorType);
			if (sensor != null) {
				mSensors.put(sensorType, sensor);		
			} else {
				Toast.makeText(this, "MISSING REQUESTED SENSOR!", Toast.LENGTH_SHORT).show();
			}
		}
		
		// open output file for writing
		mExternalRoot = android.os.Environment.getExternalStorageDirectory(); 
		mOutputDir = new File (mExternalRoot.getAbsolutePath() + "/datalogs/");
		// attempt to make output directory
		if (!mOutputDir.exists()) {
            mOutputDir.mkdirs();
        }

	}
	
	// ----- begin logging sensor data -----
	public void startLog() {
		// start to collect data
		Log.i(LOG_TAG, "start log");
		
		// start location manager
		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

		// WIFI and cell tower information are retrieved using the two BroadcastReceiver classes

		// fire up the inertial sensors
		for (int sensorType : SENSOR_TYPES) {
			mSensorManager.registerListener(this, mSensors.get(sensorType), SENSOR_DELAY);
		}

		// acquire a partial wake lock to prevent the phone from sleeping
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "DataCollectionService");
		mWakeLock.acquire();
		
		// make output file in output directory
		mOutputFile = new File(mOutputDir, sLogFile);
		// create output writers
    	try {
			fw = new FileWriter(mOutputFile.getAbsoluteFile());
		} catch (IOException e) {
			e.printStackTrace();
		}
		bw = new BufferedWriter(fw);
	} 
	
	// ----- stop logging sensor data -----
	public void stopLog() {
		// stop collecting data
		Log.i(LOG_TAG, "stop log");
		mLocationManager.removeUpdates(this);
		mSensorManager.unregisterListener(this);

		// release the wakelock
		mWakeLock.release();
		
		// close the buffered writer
		try {
			bw.close();
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onLocationChanged(Location arg0) {
		//Log.i(LOG_TAG, "New location: lac=" + arg0.getLatitude() + ", log=" + arg0.getLongitude());
		writeToExternal("100," + arg0.getTime() + "," + arg0.getLatitude() + "," + arg0.getLongitude() + "\n");
	}

	@Override
	public void onProviderDisabled(String provider) {
		
	}

	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		Log.d(LOG_TAG, "Accuracy of sensor " + sensor.getName() + " is now " + accuracy);
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		int i = 0;
		int sensorType = event.sensor.getType();
		
		// write event type and time
		writeToExternal(sensorType + "," + event.timestamp + ",");
		
		// write all but last event value
		for( i=0; i<SensorInfo.getNumberOfAxes(sensorType) -1; i++ ){
			writeToExternal(event.values[i] + ",");
		}
		// write last value with a new line
		writeToExternal(event.values[i] + "\n");
		
		// store gravity and geomag to calculate Euler angles
		if( sensorType == Sensor.TYPE_GRAVITY ){
			tmp_gravity[0] = event.values[0];
			tmp_gravity[1] = event.values[1];
			tmp_gravity[2] = event.values[2];
		}else if( sensorType == Sensor.TYPE_MAGNETIC_FIELD){
			tmp_geomag[0] = event.values[0];
			tmp_geomag[1] = event.values[1];
			tmp_geomag[2] = event.values[2];
			
			// magnetic field is a pretty slow update, so we'll use it as the bottleneck to calculate
			// the Euler angles
			getEulerAngles(tmp_eulerAngles);
			
			// write EulerAngles to file
			writeToExternal("101," + event.timestamp + "," + tmp_eulerAngles[0] + "," + tmp_eulerAngles[1] + "," + tmp_eulerAngles[2] + "\n");
		}
	}

	// ----- Checks if external storage is available for read and write -----
	public boolean isExternalStorageWritable() {
	    String state = Environment.getExternalStorageState();
	    if (Environment.MEDIA_MOUNTED.equals(state)) {
	        return true;
	    }
	    return false;
	}
	
	// ----- Write string to output log file -----
	private void writeToExternal(String data){
	    try {
	        bw.append(data);
	        bw.flush();
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	        Log.d(LOG_TAG, "!!! Output file (External storage) not found !!!");
	    } catch (IOException e) {
	        e.printStackTrace();
	    }   
	}
	
	// ----- Calculate the Euler Angles ------
	private void getEulerAngles(float[] buffer){
		// get rotation matrix
		SensorManager.getRotationMatrix(tmp_orientationRotation, tmp_orientationInclination, tmp_gravity, tmp_geomag);
		// get Euler angles
		SensorManager.getOrientation(tmp_orientationRotation, tmp_eulerAngles);
	}
	
}