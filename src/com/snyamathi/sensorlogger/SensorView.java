package com.snyamathi.sensorlogger;

import java.text.DecimalFormat;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.hardware.Sensor;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SensorView extends LinearLayout {

	private static final DecimalFormat mFormat = new DecimalFormat("+#,000.0000000000;-#");
	
	private TextView[] textViews;
	private int numberOfAxes;
	
	private SensorView(Context context) {
		super(context);
	}
	
	public SensorView(Context context, Sensor sensor) {
		super(context);
		numberOfAxes = SensorInfo.getNumberOfAxes(sensor.getType());
		
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		params.setMargins(32, 32, 32, 32);
		setPadding(16, 16, 16, 16);
		setLayoutParams(params);
		setOrientation(LinearLayout.VERTICAL);
		setBackgroundColor(Color.WHITE);		
		
		TextView name = new TextView(context);
		name.setText(sensor.getName());
		name.setGravity(Gravity.CENTER_HORIZONTAL);
		name.setTypeface(Typeface.MONOSPACE, Typeface.BOLD);		
		addView(name);
		
		textViews = new TextView[numberOfAxes];
		
		for (int i=0; i<numberOfAxes; ++i) {
			TextView axis = new TextView(context);
			axis.setText(sensor.getName());
			axis.setGravity(Gravity.CENTER_HORIZONTAL);
			axis.setTypeface(Typeface.MONOSPACE, Typeface.NORMAL);		
			
			textViews[i] = axis;
			addView(axis);
		}		
	}
	
	public void onSensorChanged(float[] values) {
		for (int i=0; i<numberOfAxes; ++i) {
			textViews[i].setText(mFormat.format(values[i]));
		}
	}
}