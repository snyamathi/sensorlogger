/*
 * ============ Package Declaration ==============
 */
package com.snyamathi.sensorlogger;

/*
 * =============== Imports =======================
 */
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

import com.snyamathi.sensorlogger.DataCollectionService.LocalBinder;


public class SensorActivity extends Activity {
	// ----- Debugging String Tag -----
		private static final String TAG = SensorActivity.class.getSimpleName();
	
	// ----- Sensor Measurement Services -----
	DataCollectionService mDataCollectionService;
	boolean mBoundDataCollection = false;
	
	private ServiceConnection mConnection = new ServiceConnection() {

	        @Override
	        public void onServiceConnected(ComponentName className, IBinder service) {
	            LocalBinder binder = (LocalBinder) service;
	            mDataCollectionService = binder.getService();
	            Log.d(TAG, "Connected to " + mDataCollectionService.toString());
	            mBoundDataCollection = true;
	            mDataCollectionService.init();
	        }

	        @Override
	        public void onServiceDisconnected(ComponentName arg0) {
	        	mBoundDataCollection = false;
	        }
	};
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Intent bindIntent = new Intent(this, DataCollectionService.class);
		//bind to the service to collect data
		bindService(bindIntent, mConnection, Context.BIND_AUTO_CREATE);

	}
	
	@Override
	public void onResume() {
		super.onResume();
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		unbindService(mConnection);
		mDataCollectionService = null;
	}
	
	/*
	 * Function: onBtnClicked
	 * 		- callback function for GUI buttons
	 */
	public void onBtnClicked(View v){
		if(v.getId() == R.id.button1){
			Log.i(TAG, "Start");
			mDataCollectionService.startLog();
		}
		if(v.getId() == R.id.button2) {
			Log.i(TAG, "Stop");
			mDataCollectionService.stopLog();
		}
  }
}